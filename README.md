# ![Node/Express/Mongoose Example App](project-logo.png)

[![Build Status](https://travis-ci.org/anishkny/node-express-realworld-example-app.svg?branch=master)](https://travis-ci.org/anishkny/node-express-realworld-example-app)

> ### Example Node (Express + Mongoose) codebase containing real world examples (CRUD, auth, advanced patterns, etc) that adheres to the [RealWorld](https://github.com/gothinkster/realworld-example-apps) API spec.

<a href="https://thinkster.io/tutorials/node-json-api" target="_blank"><img width="454" src="https://raw.githubusercontent.com/gothinkster/realworld/master/media/learn-btn-hr.png" /></a>

This repo is functionality complete — PRs and issues welcome!

## AWS Infra

[![Infra](https://file.notion.so/f/s/e6d6e62c-d25a-4098-9a48-a0d82f9551cc/realwarch.png?id=3f1cf90e-610b-4ca9-b125-d1951f540f8d&table=block&spaceId=f04603ba-065c-40c3-9d15-09519e54cfbc&expirationTimestamp=1687372909337&signature=W5yomhLdgC3fsd2NOIfuDd_D6hK7bh9OTBC36B0AJ3w&downloadName=realwarch.png)]

[![Infra](https://file.notion.so/f/s/7d7132ba-9c67-4100-b8fb-46b6e6db41ea/aws1.png?id=27a233e9-f880-45fe-aec3-32210d9accca&table=block&spaceId=f04603ba-065c-40c3-9d15-09519e54cfbc&expirationTimestamp=1687372934211&signature=84PBhZqZ-YZ37u4FsD74f6BIcwoWxAtkd4YF365MkQI&downloadName=aws1.png)]

[![Infra](https://file.notion.so/f/s/eac22614-a7d2-498d-835f-8990f81fef9d/aws2.png?id=b4f8eda0-2f3f-4b7e-bf67-1c891b6f5c20&table=block&spaceId=f04603ba-065c-40c3-9d15-09519e54cfbc&expirationTimestamp=1687372948719&signature=hYymU6zowhrPeXjj4_r9_rAXXV26m8caeHpD-yiRk4E&downloadName=aws2.png)]

[![Infra](https://file.notion.so/f/s/438e9409-f09c-4a56-980a-64bb05b36ce8/aws3.png?id=3c906b33-fc65-4d9a-b3e8-b076b92fe732&table=block&spaceId=f04603ba-065c-40c3-9d15-09519e54cfbc&expirationTimestamp=1687372960070&signature=LN4rRXygq8ciug9H7BPOP1ztQ_-diJXlqZh_DvUuRIw&downloadName=aws3.png)]

# Getting started

To get the Node server running locally:

- Clone this repo
- `npm install` to install all required dependencies
- Install MongoDB Community Edition ([instructions](https://docs.mongodb.com/manual/installation/#tutorials)) and run it by executing `mongod`
- `npm run dev` to start the local server

Alternately, to quickly try out this repo in the cloud, you can [![Remix on Glitch](https://cdn.glitch.com/2703baf2-b643-4da7-ab91-7ee2a2d00b5b%2Fremix-button.svg)](https://glitch.com/edit/#!/remix/realworld)

# Code Overview

## Dependencies

- [expressjs](https://github.com/expressjs/express) - The server for handling and routing HTTP requests
- [express-jwt](https://github.com/auth0/express-jwt) - Middleware for validating JWTs for authentication
- [jsonwebtoken](https://github.com/auth0/node-jsonwebtoken) - For generating JWTs used by authentication
- [mongoose](https://github.com/Automattic/mongoose) - For modeling and mapping MongoDB data to javascript 
- [mongoose-unique-validator](https://github.com/blakehaswell/mongoose-unique-validator) - For handling unique validation errors in Mongoose. Mongoose only handles validation at the document level, so a unique index across a collection will throw an exception at the driver level. The `mongoose-unique-validator` plugin helps us by formatting the error like a normal mongoose `ValidationError`.
- [passport](https://github.com/jaredhanson/passport) - For handling user authentication
- [slug](https://github.com/dodo/node-slug) - For encoding titles into a URL-friendly format

## Application Structure

- `app.js` - The entry point to our application. This file defines our express server and connects it to MongoDB using mongoose. It also requires the routes and models we'll be using in the application.
- `config/` - This folder contains configuration for passport as well as a central location for configuration/environment variables.
- `routes/` - This folder contains the route definitions for our API.
- `models/` - This folder contains the schema definitions for our Mongoose models.

## Error Handling

In `routes/api/index.js`, we define a error-handling middleware for handling Mongoose's `ValidationError`. This middleware will respond with a 422 status code and format the response to have [error messages the clients can understand](https://github.com/gothinkster/realworld/blob/master/API.md#errors-and-status-codes)

## Authentication

Requests are authenticated using the `Authorization` header with a valid JWT. We define two express middlewares in `routes/auth.js` that can be used to authenticate requests. The `required` middleware configures the `express-jwt` middleware using our application's secret and will return a 401 status code if the request cannot be authenticated. The payload of the JWT can then be accessed from `req.payload` in the endpoint. The `optional` middleware configures the `express-jwt` in the same way as `required`, but will *not* return a 401 status code if the request cannot be authenticated.


<br />

[![Brought to you by Thinkster](https://raw.githubusercontent.com/gothinkster/realworld/master/media/end.png)](https://thinkster.io)
